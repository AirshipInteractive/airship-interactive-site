<br /><br />

<div class="footer">
	<hr style="margin-bottom: 10px;">
	&copy; Airship Interactive <?php echo date("Y"); ?>. All rights reserved. &middot; <a href="#">Terms & Conditions</a> &middot; <a href="#">Privacy Policy</a> &middot; <a href="http://code.airshipinteractive.com" title="Where the mushy magic stuff happens">Code!</a> <br/>
	<a href="mailto:hi@airshipinteractive.com">Contact Us</a>
</div>