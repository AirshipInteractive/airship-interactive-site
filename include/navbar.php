<title>Airship Interactive</title>
<link rel="stylesheet" type="text/css" href="./assets/styles/main.css" />
</head>
<body>
<!-- Navigation bar start -->
	
<div class="navigation">

	<div class="navbar-inner">

		<a href="/"><li class="brand">Airship Interactive</li></a>

		<ul class="navbar-buttons">

			<a href="/"><li>Home</li></a>
			<a href="/blog"><li>Blog</li></a>
			<a href="/games"><li>Games</li></a>
			<a href="/staff"><li>Staff</li></a>
			<a href="#"><li>Contact</li></a>

		</ul>

	</div>

</div>

<!-- Navigation bar end -->